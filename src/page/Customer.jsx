import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllUsers, disableUserId, enableUserId } from "../slices/userSlice";
import { toast } from "react-toastify";
import Table from "../component/table/Table";

const customerHeader = [
  "",
  "name",
  "email",
  "phone",
  "gender",
  "address",
  "enabled",
  "Edit",
];
const renderHead = (item, index) => <th key={index}>{item}</th>;

const Customer = () => {
  const dispatch = useDispatch();
  const { users, loading, error } = useSelector((state) => state.user);
  console.log("here ", users);
  useEffect(() => {
    dispatch(getAllUsers());
  }, []);
  // useEffect(() => {
  //   setUserList(users);
  // }, [users]);
  const Disable = async (id) => {
    await dispatch(disableUserId(id));
    if (!error) {
      toast.success("Success");
      dispatch(getAllUsers());
    } else {
      toast.error("Error");
    }
  };
  const enable = async (id) => {
    await dispatch(enableUserId(id));
    if (!error) {
      toast.success("Success");
      dispatch(getAllUsers());
    } else {
      toast.error("Error");
    }
  };
  const renderBody = (item, index) => (
    <tr key={index}>
      <td>{item.id}</td>
      <td>{item.name}</td>
      <td>{item.email}</td>
      <td>{item.phone}</td>
      <td>{item.gender}</td>
      <td>{item.address}</td>
      <td>{item.enable === true ? "true" : "false"}</td>
      <td>
        <button
          value={item.id}
          onClick={() =>
            item.enable === true ? Disable(item.id) : enable(item.id)
          }
          style={{
            backgroundColor: "#4CAF50",
            border: "none",
            color: "white",
            padding: "20px",
            textAlign: "center",
            textDecoration: "none",
            display: "inlineBlock",
            fontSize: "16px",
            margin: "4px 2px",
            cursor: "pointer",
            borderRadius: "12px",
          }}
        >
          {item.enable === true ? "disable" : "enable"}
        </button>
      </td>
    </tr>
  );
  return (
    <div>
      <p className="page-header">Customers</p>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card__body">
              <Table
                headData={customerHeader}
                renderHead={(item, index) => renderHead(item, index)}
                bodyData={users}
                renderBody={(item, index) => renderBody(item, index)}
                // onClickUp={() => setPage(page + 1)}
                // onClickDown={() => setPage(page - 1)}
                // page={page}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Customer;
