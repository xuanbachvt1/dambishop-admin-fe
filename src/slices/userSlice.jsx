import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import userApi from "../apis/userApi";

export const getAllUsers = createAsyncThunk("auth/getAllUsers", async () => {
  const response = await userApi.getAllUsers();
  return {
    data: response.data.objList,
  };
});
export const disableUserId = createAsyncThunk(
  "auth/disableUserId",
  async (id) => {
    const { message } = await userApi.disableUserId(id);
    return message;
  }
);

export const enableUserId = createAsyncThunk(
  "auth/enableUserId",
  async (id) => {
    const { message } = await userApi.enableUserId(id);
    return message;
  }
);
const initialState = {
  users: [],
  loading: false,
  error: null,
};
export const postSlice = createSlice({
  name: "user",
  initialState,
  extraReducers: {
    [getAllUsers.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [getAllUsers.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.error;
    },
    [getAllUsers.fulfilled]: (state, action) => {
      state.loading = false;
      state.users = action.payload.data;
    },
    [disableUserId.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [disableUserId.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.error;
    },
    [disableUserId.fulfilled]: (state, action) => {
      state.loading = false;
    },
    [enableUserId.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [enableUserId.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.error;
    },
    [enableUserId.fulfilled]: (state, action) => {
      state.loading = false;
    },
  },
});

export default postSlice.reducer;
